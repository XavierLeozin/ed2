#include <stdio.h>
#include <stdlib.h>

typedef struct no {
    int info;
    struct no *esq;
    struct no *dir;
} No;

No *cria_no(int info) {
    No *novo = (No *) malloc(sizeof(No));
    novo->info = info;
    novo->esq = NULL;
    novo->dir = NULL;
    return novo;
}

No *insere(No *raiz, int info) {
    if (raiz == NULL) {
        return cria_no(info);
    }
    if (info < raiz->info) {
        raiz->esq = insere(raiz->esq, info);
    } else {
        raiz->dir = insere(raiz->dir, info);
    }
    return raiz;
}

int eh_arvore_binaria_de_busca(No *raiz) {
    if (raiz == NULL) {
        return 1;
    }
    if (raiz->esq != NULL && raiz->esq->info > raiz->info) {
        return 0;
    }
    if (raiz->dir != NULL && raiz->dir->info < raiz->info) {
        return 0;
    }
    return eh_arvore_binaria_de_busca(raiz->esq) && eh_arvore_binaria_de_busca(raiz->dir);
}

int main() {
    No *raiz = NULL;
    raiz = insere(raiz, 10);
    raiz = insere(raiz, 5);
    raiz = insere(raiz, 15);
    raiz = insere(raiz, 3);
    raiz = insere(raiz, 7);
    raiz = insere(raiz, 13);
    raiz = insere(raiz, 17);
    raiz = insere(raiz, 1);
    raiz = insere(raiz, 4);
    raiz = insere(raiz, 6);
    raiz = insere(raiz, 9);
    raiz = insere(raiz, 11);
    raiz = insere(raiz, 14);
    raiz = insere(raiz, 16);
    raiz = insere(raiz, 19);
    raiz = insere(raiz, 2);
    raiz = insere(raiz, 8);
    raiz = insere(raiz, 12);
    raiz = insere(raiz, 18);
    raiz = insere(raiz, 20);
    printf("%d\n", eh_arvore_binaria_de_busca(raiz));
    return 0;
}