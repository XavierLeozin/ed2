#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define MAX_SIZE 100

void merge(int *a, int lo, int mid, int hi);
void mergeSort(int *a, int lo, int hi);
void insertSort(int *a, int lo, int hi);
void printArray(int *a, int size);

int main(int argc, char *argv[]) {
	int a[MAX_SIZE];
	int size = 0;
	int i;
	srand(time(NULL));
	
	printf("Enter size of array: ");
	scanf("%d", &size);
	
	for(i = 0; i < size; i++) {
		a[i] = rand() % 100;
	}
	
	printf("\nUnsorted array: ");
	printArray(a, size);
	
	mergeSort(a, 0, size - 1);
	
	printf("\nSorted array: ");
	printArray(a, size);
	
	return 0;
}

void merge(int *a, int lo, int mid, int hi) {
	int i = lo;
	int j = mid + 1;
	int k;
	
	for(k = lo; k <= hi; k++) {
		if(i > mid) {
			a[k] = a[j++];
		} else if(j > hi) {
			a[k] = a[i++];
		} else if(a[i] < a[j]) {
			a[k] = a[i++];
		} else {
			a[k] = a[j++];
		}
	}
}

void mergeSort(int *a, int lo, int hi) {
	int mid;
	
	if(hi <= lo) {
		return;
	}
	
	if(hi - lo <= 10) {
		insertSort(a, lo, hi);
		return;
	}
	
	mid = lo + (hi - lo) / 2;
	mergeSort(a, lo, mid);
	mergeSort(a, mid + 1, hi);
	
	if(a[mid] <= a[mid + 1]) {
		return;
	}
	
	merge(a, lo, mid, hi);
}

void insertSort(int *a, int lo, int hi) {
	int i, j, key;
	
	for(i = lo + 1; i <= hi; i++) {
		key = a[i];
		j = i - 1;
		
		while(j >= lo && a[j] > key) {
			a[j + 1] = a[j];
			j--;
		}
		
		a[j + 1] = key;
	}
}

void printArray(int *a, int size) {
	int i;
	
	for(i = 0; i < size; i++) {
		printf("%d ", a[i]);
	}
}