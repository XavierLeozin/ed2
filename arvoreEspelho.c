#include <stdio.h>
#include <stdlib.h>
#include <time.h>

typedef struct arvore {
    int info;
    struct arvore *esq;
    struct arvore *dir;
} Arvore;

Arvore *cria_arvore(int info, Arvore *esq, Arvore *dir) {
    Arvore *a = (Arvore *) malloc(sizeof(Arvore));
    a->info = info;
    a->esq = esq;
    a->dir = dir;
    return a;
}

int verifica_espelho(Arvore *a1, Arvore *a2) {
    if (a1 == NULL && a2 == NULL) {
        return 1;
    } else if (a1 == NULL || a2 == NULL) {
        return 0;
    } else {
        return (a1->info == a2->info) && verifica_espelho(a1->esq, a2->dir) && verifica_espelho(a1->dir, a2->esq);
    }
}

Arvore *espelho(Arvore *a) {
    if (a == NULL) {
        return NULL;
    } else {
        return cria_arvore(a->info, espelho(a->dir), espelho(a->esq));
    }
}

void imprime_arvore(Arvore *a) {
    if (a != NULL) {
        printf("%d\n", a->info);
        imprime_arvore(a->esq);
        imprime_arvore(a->dir);
    }
}

int main() {
    srand(time(NULL));
    Arvore *a1 = cria_arvore(rand() % 10, cria_arvore(rand() % 10, NULL, NULL), cria_arvore(rand() % 10, NULL, NULL));
    Arvore *a2 = cria_arvore(rand() % 10, cria_arvore(rand() % 10, NULL, NULL), cria_arvore(rand() % 10, NULL, NULL));
    Arvore *a3 = espelho(a1);
    Arvore *a4 = espelho(a2);
    printf("Arvore 1:\n");
    imprime_arvore(a1);
    printf("Arvore 2:\n");
    imprime_arvore(a2);
    printf("Arvore 1 espelho:\n");
    imprime_arvore(a3);
    printf("Arvore 2 espelho:\n");
    imprime_arvore(a4);
    printf("Arvore 1 e arvore 1 espelho sao espelhos? %d\n", verifica_espelho(a1, a3));
    printf("Arvore 2 e arvore 2 espelho sao espelhos? %d\n", verifica_espelho(a2, a4));
    printf("Arvore 1 e arvore 2 sao espelhos? %d\n", verifica_espelho(a1, a2));
    printf("Arvore 1 e arvore 2 espelho sao espelhos? %d\n", verifica_espelho(a1, a4));
    printf("Arvore 1 espelho e arvore 2 sao espelhos? %d\n", verifica_espelho(a3, a2));
    printf("Arvore 1 espelho e arvore 2 espelho sao espelhos? %d\n", verifica_espelho(a3, a4));
    return 0;
}