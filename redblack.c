#define RED 1
#define BLACK 0

typedef struct arvoreRB {
    int info;
    int cor;
    struct arvoreRB *esq;
    struct arvoreRB *dir;
} ArvoreRB

int eh_no_vermelho(ArvoreRB *no) {
    if(!no) return BLACK;
    return(no->cor == RED);
}

ArvoreRB * rot_esq(ArvoreRB *no) {
    ArvoreRB * x = no->dir;
    no->dir = x->esq;
    x->esq = no;
    x->cor = no->cor;
    no->cor = RED;
    return(x);
}

ArvoreRB * rot_dir(ArvoreRB *no) {
    ArvoreRB * x = no->esq;
    no->esq = x->dir;
    x->dir = no;
    x->cor = no->cor;
    no->cor = RED;
    return(x);
}

/* Implemente as operações de inserção e remoção de nós da Árvore Red-Black apresentada. Essas operações devem manter as propriedades da Árvorve Red-Black */ 

ArvoreRB * inserir(ArvoreRB *no, int info) {
    if(!no) {
        no = (ArvoreRB *) malloc(sizeof(ArvoreRB));
        no->info = info;
        no->cor = RED;
        no->esq = no->dir = NULL;
    }
    else if(info < no->info) {
        no->esq = inserir(no->esq, info);
    }
    else if(info > no->info) {
        no->dir = inserir(no->dir, info);
    }
    else {
        printf("Erro: chave já existe\n");
        return(no);
    }

    if(eh_no_vermelho(no->dir) && !eh_no_vermelho(no->esq)) {
        no = rot_esq(no);
    }
    if(eh_no_vermelho(no->esq) && eh_no_vermelho(no->esq->esq)) {
        no = rot_dir(no);
    }
    if(eh_no_vermelho(no->esq) && eh_no_vermelho(no->dir)) {
        troca_cor(no);
    }
    return(no);
}

ArvoreRB * remover(ArvoreRB *no, int info) {
    if(!no) {
        printf("Erro: chave não existe\n");
        return(no);
    }
    else if(info < no->info) {
        no->esq = remover(no->esq, info);
    }
    else if(info > no->info) {
        no->dir = remover(no->dir, info);
    }
    else {
        if(no->esq && no->dir) {
            ArvoreRB *aux = no->esq;
            while(aux->dir) {
                aux = aux->dir;
            }
            no->info = aux->info;
            no->esq = remover(no->esq, aux->info);
        }
        else {
            ArvoreRB *aux = no;
            if(!no->esq) {
                no = no->dir;
            }
            else if(!no->dir) {
                no = no->esq;
            }
            free(aux);
        }
    }
    return(no);
}

void troca_cor(ArvoreRB *no) {
    no->cor = !no->cor;
    no->esq->cor = !no->esq->cor;
    no->dir->cor = !no->dir->cor;
}

void imprimir(ArvoreRB *no) {
    if(no) {
        imprimir(no->esq);
        printf("%d ", no->info);
        imprimir(no->dir);
    }
}

int main() {
    ArvoreRB *raiz = NULL;
    int op, info;
    do {
        printf("1 - Inserir\n");
        printf("2 - Remover\n");
        printf("3 - Imprimir\n");
        printf("0 - Sair\n");
        scanf("%d", &op);
        switch(op) {
            case 1:
                printf("Informe o valor a ser inserido: ");
                scanf("%d", &info);
                raiz = inserir(raiz, info);
                break;
            case 2:
                printf("Informe o valor a ser removido: ");
                scanf("%d", &info);
                raiz = remover(raiz, info);
                break;
            case 3:
                imprimir(raiz);
                printf("\n");
                break;
        }
    } while(op);
    return(0);
}