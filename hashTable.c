#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define TABLE_SIZE 10
#define MAX_LENGTH 50

struct node {
  char key[MAX_LENGTH];
  struct node* next;
};

struct hash_table {
  struct node* table[TABLE_SIZE];
};

unsigned int hash(char* key) {
  unsigned int h = 0;
  for (int i = 0; i < strlen(key); i++) {
    h = (h * 31) + key[i];
  }
  return h % TABLE_SIZE;
}

void init_hash_table(struct hash_table* ht) {
  for (int i = 0; i < TABLE_SIZE; i++) {
    ht->table[i] = NULL;
  }
}

void insert(struct hash_table* ht, char* key) {
  unsigned int index = hash(key);
  struct node* new_node = (struct node*)malloc(sizeof(struct node));
  strcpy(new_node->key, key);
  new_node->next = ht->table[index];
  ht->table[index] = new_node;
}

int search(struct hash_table* ht, char* key) {
  unsigned int index = hash(key);
  struct node* current = ht->table[index];
  while (current != NULL) {
    if (strcmp(current->key, key) == 0) {
      return 1;
    }
    current = current->next;
  }
  return 0;
}

void delete(struct hash_table* ht, char* key) {
  unsigned int index = hash(key);
  struct node* current = ht->table[index];
  struct node* prev = NULL;
  while (current != NULL) {
    if (strcmp(current->key, key) == 0) {
      if (prev == NULL) {
        ht->table[index] = current->next;
      } else {
        prev->next = current->next;
      }
      free(current);
      return;
    }
    prev = current;
    current = current->next;
  }
}

int main() {
  struct hash_table ht;
  init_hash_table(&ht);
  insert(&ht, "hello");
  insert(&ht, "world");
  insert(&ht, "foo");
  insert(&ht, "bar");
  if (search(&ht, "hello")) {
    printf("Found\n");
  } else {
    printf("Not Found\n");
  }
  delete(&ht, "hello");
  if (search(&ht, "hello")) {
    printf("Found\n");
  } else {
    printf("Not Found\n");
  }
  return 0;
}
